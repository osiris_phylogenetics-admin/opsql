#!/usr/bin/env python

#Created by Andrew Swafford 09-12-2016.
#Contact Andrew@Swafford.com with questions/comments.

from bruteTypeCheck import *
from bioservices import *
from Bio import SeqIO,Entrez
from ete3 import NCBITaxa
import pandas as pd
import numpy as np
import csv

####BEGIN USER INPUTS#####
accession_id_file = "testseqs.txt"

####END USER INPUTS####


ncbi = NCBITaxa()
def ncbi_specifics(convDF):
    print "Searching for NCBI specific data"
    Entrez.email = "fake@email.com"
    eSeqs = convDF[convDF.AASEQ.isnull()]  #get the sequences that have not been found yet, regardless of if they are in NCBI or not
    eSeqsL = eSeqs.index.tolist() #grab the ACC provided by the user
    size = 190 # Number of sequences to try at once, max 200 from NCBI, slightly less means less error
    chunks = [eSeqsL[i:i+size] for i  in range(0, len(eSeqsL), size)] #split the list of sequences into /size chunks
    print "Data will be accessed in ",len(chunks)," chunks"
    for o in chunks:
        entries = [str(l) for l in o] #Convert unicode into strings to avoid issues
        handle = Entrez.efetch(db='protein' , id = entries, rettype = 'gb', retmode = 'xml') #Fetch genbank info for sequences
        records = Entrez.read(handle) #handle the sequences
        print "NCBI records aquired, parsing.."
        for i in records:
            acc = i['GBSeq_primary-accession'] #acc id
            print acc
            version = i['GBSeq_accession-version'] #acc id with version #
            if version in convDF.index.tolist():
                if type(convDF.get_value(version,'AASEQ')) == float:
                    acc = version     
                #acc = version #if the user specified a version, use the specific version
            for l in i['GBSeq_feature-table'][0]['GBFeature_quals']:
                if l['GBQualifier_name'] == 'db_xref':
                    taxID = l['GBQualifier_value'].split(":")[-1] #taxon ID
            species = i['GBSeq_organism'] #species name, discard genus
            taxonomy = i['GBSeq_taxonomy'].split(";") #Lineage taxonomy from the top down to genus
            taxonomy.append(species) #add species name
            taxonomy = ", ".join(taxonomy) #unify formatting with Uniprot
            sequence = i['GBSeq_sequence'] #sequence data
            #xref = i['GBSeq_xrefs']
            convDF.set_value(acc,'SPECIES',species)
            convDF.set_value(acc,'TAXONOMY',taxonomy)
            convDF.set_value(acc,'AASEQ',sequence.upper())
    return(convDF)

u = UniProt()
typeDict = {}
with open(accession_id_file,'r') as infile: #get individual accIDs as defined by user.  Separators must be ", | \t " "" separators will be automatically detected
    sniffer = csv.Sniffer()
    x = infile.read()
    dialect = sniffer.sniff(x)
    print "IDENTIFIED ",dialect.delimiter, " AS SEPARATOR - IF THIS IS INCORRECT CANCEL NOW!!"
    x = x.split(dialect.delimiter)
    #x = infile.read().split('\n')
print "Total of ",len(x)," sequences found"
#versionList = []
#for idx,item in enumerate(x): #remove .1 from versions..subject to revision as this information may be necessary
#    if item[-2] == ".":
#        versionList.append(x[idx])
#        x[idx] = item[:-2]
x = list(set(x)) #remove duplicates
x = [i.replace("\n","") for i in x]
x = [i.replace("\r","") for i in x]
seqDict = bruteCheck(x) #perform database check on the ACCIDs
for k,v in seqDict.items():
    typeDict.setdefault(v,[]).append(k) #reverse Dictionary keys & values
print "Total of ",len(seqDict.keys())," unique sequences"
seqDF = pd.DataFrame.from_dict(seqDict,orient='index') # create a dataframe giving us the database as columns
lost = [str(i) for i in seqDF[seqDF[0].isin(['NOMATCH'])].index] # save the no match IDS for access later
typeDict.pop('NOMATCH',None) #remove nomatch IDs
types = ['P_REFSEQ_AC','ACC','EMBL','UPARC'] #set up the types we will be scanning
columns = ['P_REFSEQ_AC','ACC','EMBL','UPARC','ALT_ID','SPECIES','TAXONOMY','AASEQ']
convDF = pd.DataFrame(index = seqDF[seqDF[0] != 'NOMATCH'].index,columns=columns) #create dataframe to fill
droplist = []
uID = 0
for key,v in typeDict.items(): #fill dataframe with info from sequences available on uniprot
    print key
    fetch = [x for x in types if x != key] #list of types to map to
    for x in v:
        convDF.loc[x][key] = x #fill in the DF column corresponding to the type we are scanning (Freebies!)
    for tID in fetch:
        match = u.mapping(key,tID,v) #map accIDs using uniprots mapping API
        for i,l in match.items():
            if type(l) == list:
                l = str(",".join(l))
            value = convDF.get_value(i,tID)
            if type(value) != float: #Checks to see if the cell is already filled (required due to hard coding uniparc data.  NaN are considered floats from the numpy module (np.nan).
                continue
            if len(convDF[convDF[tID]==l]) > 0 and l != 'null':
                droplist.append(i)
                continue
            elif l[:3] == "UPI": #some issue handling UniParc accids - hard coded thier placement
                convDF.loc[i]['UPARC'] = l
            else:
                convDF.loc[i][tID] = l
droplist = list(set(droplist))
for i in droplist: #Remove duplicates in the DF, only one instance of each sequence now available
    convDF = convDF.drop(i)
print 'Done coverting'
uniP = convDF.ACC.dropna() #get rid of NaNs for uniProt ACC
uniP = uniP.where(uniP != 'null') #get rid of nulls for uniProt ACC
size = 180 #Number of ACC to query uniprot servers each request..200 is Max
chunks = [uniP[i:i+size] for i  in range(0, len(uniP), size)] #Split list into /size chunks
print "Data will be accessed in ",len(chunks)," chunks"
for i in chunks:
    entries = [str(l) for l in i]
    print 'Fetching entries'
    fun = u.get_df(entries) #get the info off of uniprot
    print 'Sorting information'
    for l in fun['Entry']:
        idx = convDF[convDF.ACC == l].index #find index in convDF corresponding to the uniprot ACC
        fdx = fun[fun.Entry == l].index.tolist()[0] #find index in the recoveredDF corresponding to the current sequence
        org = fun.get_value(fdx,'Organism ID')
        orgID = str(ncbi.get_taxid_translator([org])[org])
        taxonomy = fun.get_value(fdx,'Taxonomic lineage (ALL)')
        taxonomy = taxonomy.replace("'","")
        convDF.set_value(idx,'SPECIES',orgID)
        convDF.set_value(idx,'TAXONOMY',taxonomy) 
        convDF.set_value(idx,'AASEQ',fun.get_value(fdx,'Sequence'))
if len(convDF[convDF.AASEQ.isnull()].index.tolist()) > 0:
    convDF = ncbi_specifics(convDF) #Resolve stragglers using NCBI
convDF.replace('null',np.nan,inplace=True) #clean up dataframe for writing & later use
if len(convDF.index) > 1:
    data1 = convDF.iloc[0:len(convDF)-1]
    data2 = convDF.iloc[[len(convDF)-1]]
    data1.to_csv('OUTFILE.EQI', sep='\t', header= True, index = False)
    data2.to_csv('OUTFILE.EQI', sep='\t', header= False, index = False,mode='a',line_terminator="")
else:
    with open('OUTFILE.EQI', 'w') as outfile:
        for i in columns[:-1]: outfile.write("{0}\t".format(i))
        outfile.write("{0}\n".format(columns[-1]))
    convDF.to_csv("OUTFILE.EQI", sep="\t", header = False, mode = 'a', line_terminator = "")
if len(lost) > 0:
    with open("MANUAL_ENTRY_REQUIRED.txt",'w') as output:
        for i in lost:
            output.write("%s"%i)


import os
import re
import bioservices
from bioservices import UniProt
import pandas as pd

def is_text(seqid):
  if re.match('[A-Z,a-z,_]+$', seqid):
    return True
  return False

def is_uniprot(seqid):
  if re.match('[A-N,R-Z][0-9][A-Z][A-Z,0-9][A-Z,0-9][0-9]$', seqid):
    return 'ID'
  if re.match('[O,P,Q][0-9][A-Z,0-9][A-Z,0-9][A-Z,0-9][0-9]$', seqid):
    return 'ID'
  return False

def is_uniprot_ACCID(seqid):
    if re.match('A0A0.*$', seqid):
        return 'ACC'
    return False

def is_uniprot_variant(seqid):
  if is_uniprot(seqid[:6]):
    if len(seqid) == 6:
      return True
    variant = seqid[6:]
    if re.match('[-]\d+', variant):
      return True
  return False


def is_sgd(seqid):
  if re.match('Y[A-Z][L,R]\d\d\d[W|C]$', seqid):
    return True
  return False


def is_refseq(seqid):
  if re.match('[N,X,Y,Z][P,M]_\d+([.]\d+)?$', seqid):
    return 'P_REFSEQ_AC'
  return False

def is_uniparc(seqid):
  if seqid.startswith("UPI"):
    return 'UPARC'
  return False

def is_ensembl(seqid):
  if seqid.startswith("ENS"):
    return 'ENSEMBL_ID'
  return False

def is_embl_gb_CDS(seqid):
    if re.match('[A-T,V-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9]', seqid):
        return 'EMBL'
    return False

def is_maybe_uniprot_id(seqid):
  return '_' in seqid

def get_naked_seqid(seqid):
  if '|' not in seqid:
    return seqid
  pieces = seqid.split('|')
  if is_text(pieces[0]):
    return pieces[1]
  return seqid



type_check = ['ACC', 'EMBL', 'P_REFSEQ_AC', 'UPARC', 'ID']

def bruteCheck(list_IDs):
    dbIDdict = {}
    id_types = [
        (is_sgd, 'locustag', 'ENSEMBLGENOME_PRO_ID'),
        (is_refseq, 'refseqp', 'P_REFSEQ_AC'),
        (is_refseq, 'refseqnt', 'REFSEQ_NT_ID'),
        (is_ensembl, 'ensembl', 'ENSEMBL_ID'),
        (is_uniprot, 'uniprotid', 'ID'),
        (is_uniprot_ACCID, 'uniprotACCID', 'ACC'),
        (is_uniparc, 'uniparc', 'UPARC'),
        (is_embl_gb_CDS, 'embl', 'EMBL')]
    for seq in list_IDs:
        checklist = [f[0](seq) for f in id_types]
        checklist = list(set(checklist)- set([False]))
        if len(checklist) == 0:
            checklist = ['NOMATCH']
        dbIDdict.setdefault(seq,checklist[0])
    return(dbIDdict)

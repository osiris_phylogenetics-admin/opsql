#!/bin/bash
#This is a shell script

#First step is to use setupOpsin text file... to set up and clear database
	#call mysql with -u (user) opsinsuser -p (password) and -D (database) opsindb
		#setupOpsin.sql is a text file containing sql commands that sets the database structure

mysql -uopsinuser -p1manawa -Dopsindb < scripts/setupOpsin.sql

#Add data table of opsin publications into table called studies using perl script and tab
#delimted file
scripts/tab2sql.pl datasets/studies.txt opsindb biblio

#Add new opsin data of Bielecki et al 2014. Phylogeny adds these 2 sequences to Feuda data set
scripts/tab2sql.pl datasets/bielecki2014.txt opsindb sequence
#some taxonomies have ' which breaks sql command
perl -p -i -e "s/\'//g" datasets/Ramirez_Swafford_2.tab
scripts/tab2sql.pl datasets/Ramirez_Swafford_2.tab opsindb sequence


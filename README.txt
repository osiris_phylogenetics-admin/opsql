Collaborative Effort of the Oakley Lab members, in no particular order: 
Todd Oakley
Desmond Ramirez
Andrew Swafford

Please add to this readme as you add features.  Each feature should be given a unique header in the INDEX.
Please summarize your modules with the categories: PURPOSE, DEPENDENCIES, INPUT, OUTPUT, USAGE.  

################################

INDEX:

#1a) Equilizer
#1b) Equilizer_cmd



###############################

#1a EQUILIZER

	PURPOSE: 
	Equilizer is used to create a flatfile with synonymous accession IDs from a tab-delimited list of accession numbers.
	This tool will also pull Sequence, version, and taxonomy data when available.

	DEPENDENCIES:
	Python 2.7.x
		Biopython
		bioservices
		pandas
		numpy
		bruteTypeCheck.py (provided)
	stable internet access
		
		
	INPUT:
	A tab-delimited file with accession numbers, using numbers from Genbank or UniprotKB will limit the manual work necessary downstream.
	All typical delimiters are supported, but must be consistent - newline and carriage returns are not viewed as typical delimiters.

	OUTPUT:
	A tab-delimited file with the columns: P_REFSEQ_AC, ACC, EMBL, UPARC, TAXID, TAXONOMY, SEQUENCE.  Output files will be created with the
	name "OUTPUT.EQI" in the directory where Equilizer is located.  If particular sequences could not be retrieved, an additional file
	titles "MANUAL_ENTRY_REQUIRED" will be created.

	USAGE:
	Command line is currently not supported, but is planned for future updates.  Open Equilizer.py using a text editor and input the relevant information in
	the ###BEGIN USER INPUT### section.  While code is annotated below, it is not recommended to change any other variables beyond this section.
	
#1b EQUILIZER_CMD
	PURPOSE:
	This is the command line version of Equilizer, input, output, requirements remain the same.
	
	USAGE:
	required variables:
		-i Input file, whitespace delimited, no newlines or carriage returns
		-o Output prefix, will be placed on the beginning of all outputs.
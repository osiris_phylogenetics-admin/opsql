#!/usr/bin/perl
use strict;
use DBI;



#tab2sql takes itol formatted text file for domains and puts into vwd mysql database
#
#usage:
#tab2sql.pl <tab delimited file database table>
#first line of file is name of each field
#followed by data lines

my $database = $ARGV[1];
my $table = $ARGV[2];
my $timethru = 0;
my @field;

open INFILE, "<$ARGV[0]" or die $!;
while (<INFILE>) {
        my $currentinput = "$_";
	chomp($currentinput);
	if($timethru == 0) {
	        @field = split('\t', $currentinput);
	}else{
	        my @data = split('\t', $currentinput);
	        my $dbh = DBI->connect ("DBI:mysql:opsindb","opsinuser","1manawa");
		#Create sql command
		my $sqlcommand = "INSERT INTO ".$table." (";
		for(my $i; $i < @field; $i++) {
			$sqlcommand = $sqlcommand.$field[$i];
			if($i < @field-1) {
				$sqlcommand =  $sqlcommand.", ";
			}
		}		
		$sqlcommand = $sqlcommand.") VALUES (";
		if(@data != @field){
			die "ERROR: Data row $timethru has different number of columns than first row\n";
		}
		for(my $i; $i < @data; $i++) {
			$sqlcommand = $sqlcommand."'".$data[$i]."'";
			if($i < @data-1) {
				$sqlcommand =  $sqlcommand.", ";
			}
		}
		$sqlcommand = $sqlcommand.")";	

#	       print $sqlcommand."\n";
 	       $dbh->do("$sqlcommand");


#	        $dbh->do("insert into genes (accession, species, fullseq, annotation)
#                values
#                ('$accession','$species', '$fullseq', '$annotation')");


	}
	$timethru++;

}
close INFILE;


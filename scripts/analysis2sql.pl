#!/usr/bin/perl
#
#analysis2sql takes tab formatted text and searches for first column in 
#sequence db to get id number. Then it adds alignment and other data to mysql database
#in the analyses table
#
#usage:
#analysis2sql.pl <tab delimited file database table>
#first line of file is name of each field
#followed by data lines

use strict;
use DBI;
my $accession;
my $isoform;
my $species;
my $nvwd;
my $total=0;
my @ids;

my $database = "opsindb";
my $table = "sequence";
my $timethru = 0;
my @field;
my $query;
my $query_handle;
my $seq_id;
my $species;
my $counter =0;

open INFILE, "<$ARGV[0]" or die $!;
while (<INFILE>) {
        my $currentinput = "$_";
        chomp($currentinput);
        if($timethru == 0) {
                @field = split('\t', $currentinput);
        }else{

		#Is sequence in data base?
		# PREPARE THE QUERY
	        my $dbh = DBI->connect ("DBI:mysql:opsindb","opsinuser","1manawa");
	        my @data = split('\t', $currentinput);
		$query = "SELECT id FROM ".$table." WHERE ".$field[0]." like "."'".$data[0]."'";
		$query_handle = $dbh->prepare($query);
		$query_handle->execute();
		$query_handle->bind_columns(\$seq_id);

		while($query_handle->fetch()) {
			$counter++;
		}
		if($counter >1){
			print "WARNING: Found more than one match to \t$field[0]\t$data[0]\n";
			$counter=0;
		}
		if($seq_id eq "") {
			print "NOT FOUND\t$field[0]\t$data[0]\n";
		}else{
			#In sequences so now add into analyses
	                my $sqlcommand = "INSERT INTO ".$table." (";
	                for(my $i=1; $i < @field; $i++) {
	                        $sqlcommand = $sqlcommand.$field[$i];
	                        if($i < @field-1) {
	                                $sqlcommand =  $sqlcommand.", ";
	                        }
	                }
	                $sqlcommand = $sqlcommand.") VALUES (";
	                if(@data != @field){
	                        die "ERROR: Data row $timethru has different number of columns than first row\n";
	                }
	                for(my $i=1; $i < @data; $i++) {
	                        $sqlcommand = $sqlcommand."'".$data[$i]."'";
	                        if($i < @data-1) {
	                                $sqlcommand =  $sqlcommand.", ";
	                        }
	                }
	                $sqlcommand = $sqlcommand.")";
	              print $sqlcommand."\n";
	#               $dbh->do("$sqlcommand");
		}

	}
        $timethru++;
}
close INFILE;

#	my $sqlcommand = "UPDATE domains SET luclike = TRUE  WHERE accession LIKE '".$accession."'";

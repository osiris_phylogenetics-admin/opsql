DROP DATABASE opsindb;
CREATE DATABASE opsindb;
USE opsindb;

CREATE TABLE biblio
(
id int unsigned not null auto_increment primary key,
pubmed  varchar(20),
author1 varchar(25),
year smallint
);

CREATE TABLE sequence
(
id int unsigned not null auto_increment primary key,
p_refseq_ac varchar(256),
acc varchar(256),
embl varchar(256),
uparc varchar(256),
alt_id varchar(256),
species varchar(100),
taxonomy mediumtext,
notes mediumtext,
aaseq varchar(1500)
);

CREATE TABLE analyses
(
id int unsigned not null auto_increment primary key,
biblio_id int,
sequence_id int,
aligned varchar(3000),
names varchar(256),
clades varchar(256),
dataset varchar(100),
notes mediumtext
);

CREATE TABLE unpublished
(
id int unsigned not null auto_increment primary key,
user varchar(25),
temp_name varchar(25),
species varchar(100),
aaseq varchar(1500),
treatment varchar(25),
source varchar(100),
notes varchar(1500)
);
